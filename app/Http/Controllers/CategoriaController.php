<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    public function index()
    {
        $categorias = Categoria::paginate(10);
        return view("categorias.index")->with("categorias", $categorias);
    }

    public function create()
    {
        return view("categorias.create");
    }

    public function store(Request $r)
    {

        if ($r->nombrecategoria != null) {

            $reglas = [
                "categoria" => ["required"],
            ];
            $mensajes = [
                "required" => "Este campo es obligatorio",
                "alpha" => "Solo letras",
            ];
            $validator = Validator::make($r->all(), $reglas, $mensajes);

            if ($validator->fails()) {
                return redirect("categorias/create")->withErrors($validator);
            } else {
                return redirect("categorias/create")->with("mensaje", "Categoria agregada correctamente!");
            }
            // $categoria = new Categoria();
            // $categoria->name = $request->nombrecategoria;
            // $categoria->save();
            // return back();
        } else {

            return back();
        }
    }

    public function edit($idcategory)
    {
        $category = Categoria::find($idcategory);

        return view("categorias/edit")->with("categoria", $category);
    }

    public function update($category_id)
    {
        $categoria = Categoria::find($category_id);
        $categoria->name = $_POST["nombrecategoria"];
        $categoria->save();

        return redirect("categorias/edit/$category_id")->with("mensaje", "Categoria Actualizada");
    }

    public function delete()
    {
    }
}
