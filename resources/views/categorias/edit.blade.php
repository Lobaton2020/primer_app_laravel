@include('layouts.header')

    <div class="container">
       <div class="row">
           <div class="col-md-2 mt-3"></div>
           <div class="col-md-8">
               @if(session("mensaje"))
                    <p class="alert alert-success">{{ session("mensaje") }}</p>
                @endif
           
               <form action="{{ url("categorias/update/$categoria->category_id") }}" method="POST">
                {{-- <input type="hidden" name="idcategory" value="{{ $categoria->category_id }}"> --}}
                    @csrf
                    <h3 class="text-center mt-4">Actualizar categorias</h3>
                    <a href="{{ url("categorias") }}">Ver categorias</a>
                    <div class="form-group">
                    <input name="nombrecategoria" type="text" placeholder="Ingresa la categoria" value="{{ $categoria->name }}" class="form-control">
                        {{-- <strong class="text-danger">{{ $errors->first("categoria") }}</strong> --}}
                    </div>  
                    <div class="form-group">
                        <button  type="submit" class="btn btn-primary btn-block">Registrar Categoria</button>
                    </div>
                    </form>
                    

                 </div>
            <div class="col-md-2"></div>
        </div>
    </div>

@include('layouts.footer')