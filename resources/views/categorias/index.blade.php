
@include("layouts.header")
<div class="container">
     <h1 class="text-center" >Welcom to this view</h1>
      <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
            <a href="{{ url("categorias/create") }}">Crear categoria</a>
                <table class="table table-bordered">
                     <tr>
                         <th>#</th>
                        <th>Nombre de la categoria</th>
                        <th>Acciones</th>
                     </tr>
                 @foreach ($categorias as $categoria)
                     <tr>
                         <td>{{ $categoria->category_id }}</td>
                        <td>{{ $categoria->name }}</td>
                         <td><a href="{{ route("categoria.edit",[ $categoria->category_id]) }}">Editar</a></td> 
                    </tr>    
                 @endforeach
                </table>
           {{ $categorias->links() }}
            </div>
           
           <div class="col-md-3"></div>
      </div>
</div>
@include("layouts.footer")

