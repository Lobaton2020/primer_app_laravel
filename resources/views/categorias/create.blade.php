@include('layouts.header')

    <div class="container">
       <div class="row">
           @if(session("mensaje")):
                <p>{{ session("mensaje") }}</p>
            @endif
       
            <div class="col-md-2 mt-3"></div>
               <div class="col-md-8">
                   <form action="{{ url("categorias/store") }}" method="POST">
                    @csrf
                    <h3 class="text-center mt-4">Nueva categoria</h3>
                    <a href="{{ url("categorias") }}">Ver categorias</a>
                    <div class="form-group">
                        <input name="nombrecategoria" type="text" placeholder="Ingresa la categoria" class="form-control">
                        <strong class="text-danger">{{ $errors->first("categoria") }}</strong>
                    </div>  
                    <div class="form-group">
                        <button  type="submit" class="btn btn-primary btn-block">Registrar Categoria</button>
                    </div>
                    </form>
                    

                 </div>
            <div class="col-md-2"></div>
        </div>
    </div>

@include('layouts.footer')